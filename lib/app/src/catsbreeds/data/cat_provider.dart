import 'package:flutter/material.dart';
import 'package:catsbreeds/app/src/catsbreeds/data/cat_service.dart';
import 'package:catsbreeds/app/src/catsbreeds/domain/cat_breed_model.dart';

class CatProvider extends ChangeNotifier {
  final _service = CatService();
  List<CatBreed> _catBreedList = [];
  bool isLoading = true;

  List<CatBreed> get catBreedList => _catBreedList;

  Future<void> getCatsBreeds() async {
    isLoading = true;
    _catBreedList.clear();
    await _service
        .getCatsBreeds()
        .then((value) => _catBreedList = value)
        .whenComplete(() => isLoading = false);
    notifyListeners();
  }
}
