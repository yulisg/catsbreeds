import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:catsbreeds/app/api/api.dart';
import 'package:catsbreeds/app/utils/utils.dart' as util;
import 'package:catsbreeds/app/src/catsbreeds/domain/cat_breed_model.dart';

class CatService {
  final List<CatBreed> _catBreedList = [];
  String _urlImage = "";

  Future<List<CatBreed>> getCatsBreeds() async {
    try {
      final urlBreeds = Uri.parse(Api.breeds);
      final resp = await http.get(urlBreeds, headers: {'x-api-key': Api.key});
      _catBreedList.clear();
      if (resp.statusCode == 200) {
        final decoded = json.decode(resp.body);
        for (var i = 0; i < decoded.length; i++) {
          final referenceImageId = decoded[i]["reference_image_id"];
          _catBreedList.add(CatBreed.fromJson(decoded[i]));
          if (referenceImageId != null) {
            final urlImage = await getUrlImage(referenceImageId);
            _catBreedList[i].urlImage = urlImage;
          }
        }
      }
    } catch (e) {
      util.errorSnackBar("Error get the data");
    }
    return _catBreedList;
  }

  Future<String> getUrlImage(String referenceImageId) async {
    final url = Uri.parse(Api.image + referenceImageId);
    final resp = await http.get(url, headers: {'x-api-key': Api.key});
    if (resp.statusCode == 200) {
      final decoded = json.decode(resp.body);
      _urlImage = decoded["url"];
    }
    return _urlImage;
  }
}
