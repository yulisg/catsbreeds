import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:catsbreeds/app/src/catsbreeds/data/cat_provider.dart';
import 'package:catsbreeds/app/src/catsbreeds/ui/widgets/cat_list.dart';
import 'package:catsbreeds/app/src/catsbreeds/domain/cat_breed_model.dart';

class SearchCatDelegate extends SearchDelegate {
  @override
  String get searchFieldLabel => "Search cat";

  @override
  ThemeData appBarTheme(BuildContext context) {
    return ThemeData(
      appBarTheme: const AppBarTheme(backgroundColor: Colors.blue),
      colorScheme: const ColorScheme.light(primary: Colors.white),
      textTheme: const TextTheme(
        titleLarge: TextStyle(color: Colors.white, fontSize: 18.0),
      ),
      inputDecorationTheme: const InputDecorationTheme(
        hintStyle: TextStyle(color: Colors.white),
      ),
    );
  
  }

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => query = '',
        icon: const Icon(Icons.clear),
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () => close(context, null),
      icon: const Icon(Icons.arrow_back),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final catProRead = context.read<CatProvider>();
    List<CatBreed> results = catProRead.catBreedList.where((catBreed) {
      final result = catBreed.name.toLowerCase();
      final input = query.toLowerCase();
      return result.contains(input);
    }).toList();
    return Scaffold(
      backgroundColor: Colors.blueGrey[50],
      body: CatList(results));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }
}
