import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:catsbreeds/app/src/catsbreeds/ui/widgets/cat_text.dart';
import 'package:catsbreeds/app/src/catsbreeds/domain/cat_breed_model.dart';

class CatDetailPage extends StatelessWidget {
  const CatDetailPage({required this.catBreed, super.key});
  final CatBreed catBreed;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(catBreed.name),
        elevation: 0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CachedNetworkImage(
            imageUrl: catBreed.urlImage,
            height: 350.0,
            width: double.infinity,
            fit: BoxFit.cover,
            placeholder: (context, url) => Container(color: Colors.black12),
            errorWidget: (context, url, error) => Container(
              color: Colors.black12,
              child: const Icon(Icons.error, color: Colors.red),
            ),
          ),
          Expanded(
            child: Scrollbar(
              thumbVisibility: true,
              trackVisibility: true,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 15.0,
                    bottom: 25.0,
                    left: 25.0,
                    right: 25.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CatText(catBreed.description),
                      const SizedBox(height: 10.0),
                      CatText("Country: ${catBreed.origin}"),
                      const SizedBox(height: 10.0),
                      CatText("Intelligence: ${catBreed.intelligence}"),
                      const SizedBox(height: 10.0),
                      CatText("Adaptability: ${catBreed.adaptability}"),
                      const SizedBox(height: 10.0),
                      CatText("Time of life: ${catBreed.lifeSpan}"),
                      const SizedBox(height: 10.0),
                      CatText("Temperament: ${catBreed.temperament}"),
                      const SizedBox(height: 10.0),
                      CatText("Affection_level: ${catBreed.affectionLevel}"),
                      const SizedBox(height: 10.0),
                      CatText("Dog friendly: ${catBreed.dogFriendly}"),
                      const SizedBox(height: 10.0),
                      CatText("Energy level: ${catBreed.energyLevel}"),
                      const SizedBox(height: 10.0),
                      CatText("Health issues: ${catBreed.healthIssues}")
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
