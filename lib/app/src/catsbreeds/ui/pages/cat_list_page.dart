import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:animate_do/animate_do.dart';
import 'package:catsbreeds/app/src/catsbreeds/data/cat_provider.dart';
import 'package:catsbreeds/app/src/catsbreeds/ui/widgets/cat_list.dart';
import 'package:catsbreeds/app/src/catsbreeds/ui/delegates/search_cat_delegate.dart';

class CatListPage extends StatefulWidget {
  const CatListPage({super.key});

  @override
  State<CatListPage> createState() => _CatListPageState();
}

class _CatListPageState extends State<CatListPage> {

  @override
  void initState() {
    final catProRead = context.read<CatProvider>();
    catProRead.getCatsBreeds();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final catProNotify = context.watch<CatProvider>();
    return WillPopScope(
      onWillPop: () async {
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            children: const [
              Icon(Icons.pets),
              SizedBox(width: 15.0),
              Text("Cats breeds"),
            ],
          ),
          automaticallyImplyLeading: false,
          elevation: 0,
          actions: [
            FadeIn(
              animate: catProNotify.catBreedList.isNotEmpty,
              duration: const Duration(milliseconds: 800),
              child: IconButton(
                icon: const Icon(Icons.search),
                onPressed: () async {
                  await showSearch(
                    context: context,
                    delegate: SearchCatDelegate(),
                  );
                },
              ),
            ),
          ],
        ),
        backgroundColor: Colors.blueGrey[50],
        body: (catProNotify.isLoading)
            ? const Center(child: CircularProgressIndicator())
            : (catProNotify.catBreedList.isEmpty)
                ? const Center(child: Text("No information"))
                : CatList(catProNotify.catBreedList),
      ),
    );
  }
}
