import 'package:flutter/material.dart';
import 'package:catsbreeds/app/routes/route_names.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:catsbreeds/app/src/catsbreeds/ui/widgets/cat_text.dart';
import 'package:catsbreeds/app/src/catsbreeds/domain/cat_breed_model.dart';

class CatList extends StatefulWidget {
  const CatList(this.catBreedList, {super.key});
  final List<CatBreed> catBreedList;
  @override
  State<CatList> createState() => _CatListState();
}

class _CatListState extends State<CatList> {
  late ScrollController _scrollController;
  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: _scrollController,
      shrinkWrap: true,
      slivers: [
        SliverList(
          delegate: SliverChildBuilderDelegate((context, i) {
            if (i == widget.catBreedList.length) {
              return const Padding(padding: EdgeInsets.only(bottom: 40.0));
            }
            final catBreed = widget.catBreedList[i];
            return InkWell(
              onTap: () {
                Navigator.pushNamed(
                  context,
                  RouteNames.catDetail,
                  arguments: [catBreed],
                );
              },
              child: _CardCat(catBreed),
            );
          }, childCount: widget.catBreedList.length + 1),
        )
      ],
    );
  }
}

class _CardCat extends StatelessWidget {
  const _CardCat(this.catBreed);
  final CatBreed catBreed;
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
      elevation: 4,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 15.0,
              bottom: 15.0,
              left: 25.0,
              right: 15.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: CatText(catBreed.name)),
                const CatText("More"),
                const Icon(Icons.keyboard_arrow_right),
              ],
            ),
          ),
          Visibility(
            visible: catBreed.urlImage.isNotEmpty,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: CachedNetworkImage(
                imageUrl: catBreed.urlImage,
                height: 200.0,
                width: 200.0,
                fit: BoxFit.cover,
                placeholder: (context, url) => Container(color: Colors.black12),
                errorWidget: (context, url, error) => Container(
                  color: Colors.black12,
                  child: const Icon(Icons.error, color: Colors.red),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 15.0,
              bottom: 15.0,
              left: 25.0,
              right: 15.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CatText(catBreed.origin),
                CatText("Intelligence: ${catBreed.intelligence}"),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
