import 'package:flutter/material.dart';
import 'package:catsbreeds/app/app.dart';

void errorSnackBar(String msg) {
  scaffoldMessengerKey.currentState!.clearSnackBars();
  final snackBar = SnackBar(
    backgroundColor: Colors.red,
    behavior: SnackBarBehavior.fixed,
    content: Row(
      children: [
        const Icon(Icons.cancel, color: Colors.white),
        const SizedBox(width: 15.0),
        Expanded(child: Text(msg, style: const TextStyle(fontSize: 15))),
      ],
    ),
  );
  scaffoldMessengerKey.currentState!.showSnackBar(snackBar);
}