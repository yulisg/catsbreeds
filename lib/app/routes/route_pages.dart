import 'package:flutter/material.dart';
import 'package:catsbreeds/app/routes/route_names.dart';
import 'package:catsbreeds/app/src/catsbreeds/ui/pages/cat_list_page.dart';
import 'package:catsbreeds/app/src/catsbreeds/ui/pages/cat_detail_page.dart';

class RoutePages {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    List<dynamic> args = [];
    if (settings.arguments != null) args = settings.arguments! as List<dynamic>;
    switch (settings.name) {
      case RouteNames.catDetail:
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => CatDetailPage(catBreed: args[0]),
        );
      case RouteNames.catList:
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => const CatListPage(),
        );
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (context) => Scaffold(
            body: Center(
              child: Text("Undefined route ${settings.name!}"),
            ),
          ),
        );
    }
  }
}
