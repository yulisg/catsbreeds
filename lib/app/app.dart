import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:catsbreeds/app/routes/route_names.dart';
import 'package:catsbreeds/app/routes/route_pages.dart';
import 'package:catsbreeds/app/src/catsbreeds/data/cat_provider.dart';

final scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

class App extends StatelessWidget {
  const App({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
         ChangeNotifierProvider(create: (_) => CatProvider()),
      ],
      child: MaterialApp(
        title: 'CatsBreeds',
        scaffoldMessengerKey: scaffoldMessengerKey,
        debugShowCheckedModeBanner: false,
        initialRoute: RouteNames.catList,
        onGenerateRoute: RoutePages.generateRoute,
      ),
    );
  }
}
