import 'package:flutter/material.dart';
import 'package:catsbreeds/app/app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const App());
}

